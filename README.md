# Ansible Generate SSH config

WARNING: This repository is only for lazy developers! 

Generating .ssh/config using ansible.

## Getting started

### Prerequisities

- ansible

### Generate ssh config

#### Example
Generate ssh config file with basic values
```
ansible-playbook playbook.yml -i example_inventory.yml 
```
This create in our repo `config` file with all hosts from inventory.

#### Example 2
Generate ssh config with custom inventory and also rewrite basic variable ssh_config_filepath.
`ssh_config_filepath` describing filepath to destination file where we want append new aliases.
```
ansible-playbook playbook.yml -i /my/custom/inventory.yml -e "ssh_config_filepath=/home/user/.ssh/config" 
```

### Support keys in ssh config you can see in 

In your host definition in Inventory file, you can use these keys with value.
You can check `templates/ssh_config.j2` to see which keys are supported in generating aliases.

- `ansible_port` - add port number to your host (default 22)
- `ansible_ssh_private_key_file` - add path to private ssh key for host 
- `comment` - just add comment with your message to your ssh config, you can use it as describe your host additionaly
- `ssh_proxy_command`
- `ssh_local_forward`
- `ssh_protocol`
- `ssh_server_alive_interval`
- `ssh_server_alive_count_max`
- `ssh_strict_host_key_checking`
